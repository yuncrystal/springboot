package yunkrystal.com.crudDemo.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import yunkrystal.com.crudDemo.entity.User;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *  tutorial video: https://www.youtube.com/watch?v=X80nJ5T7YpE&t=291s
 *  jjwt :https://github.com/jwtk/jjwt#jws-create-claims-standard
 */

public class JwtUtil {

    private String SECRET_KEY = "secret";


    public String generateToken(User user){
        Map<String,Object> claims = new HashMap<>();
        return createToken(claims,user.getUsername());
    }

    private String createToken(Map<String,Object> claims, String subject){
        return  Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.ES256,SECRET_KEY).compact();
    }
}
