//package yunkrystal.com.crudDemo.dao;
//import org.hibernate.Session;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//import yunkrystal.com.crudDemo.entity.User;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//@Repository
//public class UserDaoImpl implements UserDAO{
//
//    private EntityManager entityManager;
//
//    @Autowired
//    public UserDaoImpl(EntityManager theEntityManager) {
//        this.entityManager = theEntityManager;
//    }
//
//    @Override
//    public List<User> findAll() {
//
////        Get current hibernate session
//        Session session = entityManager.unwrap(Session.class);
//
////        Create a query
//        Query<User> theQuery = session.createQuery("from User", User.class);
//        List<User> users = theQuery.getResultList();
//
//        return users;
//    }
//
//    @Override
//    public User findUserById(int id) {
//        Session session = entityManager.unwrap(Session.class);
//        User use = session.get(User.class,id);
//        return use;
//    }
//
//    @Override
//    public List<User> findUserByUsername(String username) {
//        Session session = entityManager.unwrap(Session.class);
//        Query<User> theQuery = session.createQuery("from User u where u.username =:username",User.class);
//        theQuery.setParameter("username",username);
//        List<User> users  = theQuery.getResultList();
//        return users;
//    }
//
//    @Override
//    public void deleteUser(int id) {
//
//    }
//
//    @Override
//    public void save(User theUser) {
//
//    }
//}
