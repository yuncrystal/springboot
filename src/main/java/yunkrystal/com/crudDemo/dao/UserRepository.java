package yunkrystal.com.crudDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yunkrystal.com.crudDemo.entity.User;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Integer> {


    @Query("from User u where u.username=:username")
    User findByUsername(@Param("username") String username);


    @Query("from User u where u.firstName like %?1%")
    List<User> findByFirstName(@Param("username") String firstName);


    List<User> findByLastName(String lastName);
}
