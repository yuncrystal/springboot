package yunkrystal.com.crudDemo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import yunkrystal.com.crudDemo.dao.UserRepository;
import yunkrystal.com.crudDemo.entity.User;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserRestController {


    @Autowired
    UserRepository userRepository;

    @GetMapping("/users/username/{username}")
    public User findUserByUsername(@PathVariable String username){
        User users = userRepository.findByUsername(username);

        if(users == null ){
            throw new RuntimeException("User not find -"+ username);
        }
        return users;
    }

    @GetMapping("/users/firstName/{firstName}")
    public List<User> findUserByFirstName(@PathVariable String firstName){
        List<User> users = userRepository.findByFirstName(firstName);
        if(users == null ){
            throw new RuntimeException("User not find -"+ firstName);
        }
        return users;
    }

    @GetMapping("users/lastName/{lastName}")
    public List<User> findByLastName(@PathVariable String lastName){
        List<User> users= userRepository.findByLastName(lastName);
        return users;
    }
}
