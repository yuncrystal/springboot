//package yunkrystal.com.crudDemo.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import yunkrystal.com.crudDemo.dao.UserDAO;
//import yunkrystal.com.crudDemo.entity.User;
//
//import java.util.List;
//
//
//@Service
//public class UserServiceImpl implements UserService{
//
//    private UserDAO userDao;
//
//    @Autowired
//    public UserServiceImpl(UserDAO theUserDao){
//        userDao = theUserDao;
//    }
//
//    @Override
//    @Transactional
//    public List<User> findAll() {
//        return userDao.findAll();
//    }
//
//    @Override
//    @Transactional
//    public User findUserById(int id) {
//        return userDao.findUserById(id);
//    }
//
//    @Override
//    public List<User> findUserByUsername(String username) {
//        return userDao.findUserByUsername(username);
//    }
//
//    @Override
//    @Transactional
//    public void deleteUser(int id) {
//
//    }
//
//    @Override
//    @Transactional
//    public void save(User theUser) {
//
//    }
//}
