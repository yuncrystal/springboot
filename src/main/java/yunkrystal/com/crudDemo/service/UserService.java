//package yunkrystal.com.crudDemo.service;
//
//import yunkrystal.com.crudDemo.entity.User;
//
//import java.util.List;
//
//public interface UserService {
//
//    public List<User> findAll();
//    public User findUserById(int id);
//    public List<User> findUserByUsername(String username);
//    public void deleteUser(int id);
//    public void save(User theUser);
//}
